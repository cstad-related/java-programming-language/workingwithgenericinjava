package model;

public class Worker extends Person {
    private float hours;
    private float wages;
    private String position;
    public Worker(int id, String name, String gender , int age, String address, float hours, float wages, String position ) {
        super(id, name, gender, age, address);
        this.hours = hours;
        this.wages = wages;
        this.position = position;
    }

    public float getHours() {
        return hours;
    }

    public void setHours(float hours) {
        this.hours = hours;
    }

    public float getWages() {
        return wages;
    }

    public void setWages(float wages) {
        this.wages = wages;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
