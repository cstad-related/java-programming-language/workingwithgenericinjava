package model;

public class Student extends Person {
    private float score;
    private String className;

    public Student() {

    }

    @Override
    public String toString() {

        return "Student{" +
                super.toString() +
                "score=" + score +
                ", className='" + className + '\'' +
                '}';
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Student(int id, String name, String gender, int age, String address, float score, String className) {
        super(id, name, gender, age, address);

        this.score = score;
        this.className = className;
    }
}
