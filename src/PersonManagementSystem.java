import model.Person;
import model.Student;
import model.Worker;
import utils.TableUtils;

import java.util.ArrayList;
import java.util.List;

public class PersonManagementSystem {
    public static void main(String[] args) {

        List<Student> studentList = new ArrayList<>(){{
            for(int i = 1 ; i<=10; i++){
                add(new Student(100+i,"student"+i,"male", 21,"cambodia",100,"Data Analytic"));
            }
        }};
        List<Person> personList = new ArrayList<>(){{
            for(int i = 1 ; i <= 5; i++){
                add(new Person(100+i,"person"+i,"male", 21,"cambodia"));
            }
        }};
        List<Worker> workerList = new ArrayList<>(){{
            for(int i = 1 ; i <= 5; i++){
                add(new Worker(100+i,"worker"+i,"male", 21,"cambodia", 8, 1000, "Developer"));
            }
        }};

        TableUtils.renderObjectToTable(personList);
        TableUtils.renderObjectToTable(studentList);
        TableUtils.renderObjectToTable(workerList);
    }
}
