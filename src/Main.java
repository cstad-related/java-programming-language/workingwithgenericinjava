import model.Person;
import utils.BaseResponse;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Main {

    static <T> void displayInformation (T data ){
        System.out.println("---------<<Information>>----------------");
        System.out.println(data );
    }
    public static void main(String[] args) {

        displayInformation("Hello World!!! ");
        displayInformation(23456789);
        displayInformation(true);
        displayInformation(new Person(1001,"Bunny","male",23,"cambodia"));

      /*  // ? wildcard
        BaseResponse<Person> personResponse = new BaseResponse<>();
        personResponse.setMessage("Successfully created person");
        personResponse.setPayload(new Person(1001,"john","male",27,"usa"));
        personResponse.setStatus(200);


        BaseResponse<List<Person>> getAllPersonResponse = new BaseResponse<>();
        getAllPersonResponse.setPayload(new ArrayList<Person>(){{
            add(new Person(1001,"john","male",27,"usa"));
        }});

        System.out.println(personResponse);
*/
    }
}
